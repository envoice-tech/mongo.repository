namespace Envoice.MongoRepository.IntegrationTests.Tests
{
    /// <summary>
    /// Test collections
    /// </summary>
    public static class Collections
    {
        /// <summary>
        /// Used to run all database tests sequentially in support
        /// of having a clean database per-test
        /// </summary>
        public const string Database = "Database";
    }
}
