### Envoice 

# MongoRepository [![Build status](https://ci.appveyor.com/api/projects/status/09c4fnv2ov54vpwm?svg=true)](https://ci.appveyor.com/project/christophla/conditions)

MongoDB repository abstraction library.

## Supported Platforms
* .NET 2.0

## Installation

https://www.myget.org/feed/envoice/package/nuget/Envoice.MongoRepository

Add dependency to you project.json:

``` bash

dotnet add package Envoice.MongoRepository --version 1.0.0 --source https://www.myget.org/F/envoice/api/v3/index.json
```
